var i2c = require('i2c');
// $ sudo i2cdetect -y 0
var address = 0x48;
var sensor = new i2c(address, {device: '/dev/i2c-0'});
var Promise = require('promise');

// read from ADT7410
exports.getTemperature = function() {
    var promise = new Promise(function (resolve, reject) {
	sensor.readBytes(0x00, 2, function(err, data) {
	    if (err) {
		reject(err);
		return;
	    }
	    var temp, value;
	    temp = (data[0] << 8 | data[1]) >> 3;
	    if (temp >= 4096) {
		temp -= 8192;
	    }
	    value = temp * 0.0625;
	    //console.log("Temperature: " + value + " [Deg. C.]");
	    resolve(value);
	});
    });
    return promise;
};
