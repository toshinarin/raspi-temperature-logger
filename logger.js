var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('temperature.sqlite3');
var Promise = require('promise');

exports.log = function(temperature) {
    var promise = new Promise(function (resolve, reject) {
	db.serialize(function(){
	    db.run("INSERT INTO temperature VALUES (datetime('now'), (?))", [temperature]);
	    resolve();
	}); 
    });
    return promise;
}

exports.getLog = function() {
    var promise = new Promise(function (resolve, reject) {
	db.serialize(function(){
	    db.all("SELECT timestamp, temp FROM temperature", function(err, rows){
		if (err) {
		    reject(err);
		    return;
		}
		resolve(rows);
	    }); 
	}); 
    });
    return promise;
}
