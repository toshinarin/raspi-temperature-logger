## Summary

* Room Temperature Logger
* 0.0.1

## Setup

* node.js

```
$ wget http://node-arm.herokuapp.com/node_0.10.36_armhf.deb
$ sudo dpkg -i node_0.10.36_armhf.deb
```

* i2c
```
$ sudo nano /etc/modules
# add i2c-dev
$ sudo nano /boot/config.txt
# add dtparam=i2c_arm=on
$ sudo reboot
$ sudo adduser pi i2c
$ sudo apt-get install i2c-tools
```

## References
* [Raspberry PiによるIoT（M2M）【I2C温度センサー/xively/Python】](http://yamaryu0508.hatenablog.com/entry/2014/08/19/233431)
* [Raspberry Pi の I2C を有効化する方法 (2015年版)](https://blog.ymyzk.com/2015/02/enable-raspberry-pi-i2c/)