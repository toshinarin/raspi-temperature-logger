#!/bin/bash

DBFILE="temperature.sqlite3"
echo "setup start..."
#sudo apt-get install sqlite3
if [ ! -f $DBFILE ]; then
    sqlite3 temperature.sqlite3 "create table temperature(timestamp DATETIME, temp NUMERIC);"
    echo "${DBFILE} created."
fi

#wget http://node-arm.herokuapp.com/node_latest_armhf.deb
#sudo dpkg -i node_latest_armhf.deb

#node-i2c incompatible 0.12.0 (on 2015/02/15)
#wget http://node-arm.herokuapp.com/node_0.10.36_armhf.deb
#sudo dpkg -i node_0.10.36_armhf.deb

echo "finish."
