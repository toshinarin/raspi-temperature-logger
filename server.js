var sensor = require('./adt7410.js');
var logger = require('./logger.js');
function recordTemperature() {
    sensor.getTemperature().then(function(value){
	logger.log(value).then(function(){
	});
    }, function(err){
	console.log('error: ' + err);
    });
}
recordTemperature();
setInterval(function(){
    recordTemperature();
}, 60 * 1000)

var connect = require('connect');
var http = require('http');
var app = connect();
var serveStatic = require('serve-static');
app.use(serveStatic('htdocs', {'index': 'index.html'}));
app.use('/log', function barMiddleware(req, res, next) {
    logger.getLog().then(function(log){
	res.end(JSON.stringify(log));
    });
});
http.createServer(app).listen(3000);